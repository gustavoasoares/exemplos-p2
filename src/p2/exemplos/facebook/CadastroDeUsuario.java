package p2.exemplos.facebook;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CadastroDeUsuario {
	
	private List<Usuario> usuarios;
	
	public CadastroDeUsuario(int quantidadeMaxima) {
		usuarios = new ArrayList<>();
	}
	
	public void adicionaUsuario(Usuario usuario) throws Exception {
		if (usuarioJaExiste(usuario)) {
			throw new Exception("Usu�rio j� cadastrado");
		}
		usuarios.add(usuario);	
	}
	
	private boolean usuarioJaExiste(Usuario usuario) {
		return buscaUsuarioPorEmail(usuario.getEmail()) != null;
	}
	
	
	
	public Usuario buscaUsuarioPorEmail(String email) {
//		for (Usuario usuario : usuarios) {
//			if (usuario.getEmail().equals(email))
//				return usuario;
//		}
		
//		for (int indexUsuario = 0; indexUsuario < usuarios.size(); indexUsuario++) {
//			Usuario usuarioAtual = usuarios.get(indexUsuario);
//			if (usuarioAtual.getEmail().equals(email))
//				return usuarioAtual;
//		}
		
		Iterator<Usuario> iterator = usuarios.iterator();
		while (iterator.hasNext()) {
			Usuario usuarioAtual = iterator.next();
			
			if (usuarioAtual.getEmail().equals(email))
				return usuarioAtual;
		}
		
		return null;
	}

}
