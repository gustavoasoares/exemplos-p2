package p2.exemplos.facebook;

public class Usuario {
	
	private String nome;
	private String email;
	private String senha;

	public Usuario(String nome, String email, String senha) {
		this.nome = nome;
		this.email = email;
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public String getEmail() {
		return email;
	}

	public String getSenha() {
		return senha;
	}
	
	
	public boolean equals(Object objetoComparado) {
		if (!(objetoComparado instanceof Usuario))
			return false;
		
		Usuario usuarioComparado = (Usuario) objetoComparado;
		
		if (!this.email.equals(usuarioComparado.getEmail()))
			return false;
		
		return true;
		
	}
	
	

}
