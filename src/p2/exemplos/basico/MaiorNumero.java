package p2.exemplos.basico;


public class MaiorNumero {
	
	public static void main(String[] args) {
	
		java.util.Scanner sc = new java.util.Scanner(System.in);
		
		System.out.print("Digite o valor para o n�mero 1: ");
		int numero1 = sc.nextInt();
		System.out.print("Digite o valor para o n�mero 2: ");		
		int numero2 = sc.nextInt();
		System.out.print("Digite o valor para o n�mero 3: ");
		int numero3 = sc.nextInt(); 
		
		if (numero1 > numero2 && numero1 > numero3) {
				System.out.println("N�mero 1 � o maior");
		} else if (numero2 > numero3) {
			System.out.println("N�mero 2 � o maior");
		} else {
			System.out.println("N�mero 3 � o maior");
		}
		
	}

}
