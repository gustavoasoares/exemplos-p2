package p2.exemplos.basico;

public class DiasDaSemanaNovo {

	public static void main(String[] args) {

		if(args.length != 1) {
			System.err.println("Sintaxe incorreta: use DiaDaSemana <dia>");
			System.exit(1);
		}
		
		final String[] diasDasemana = {"", "Domingo", "Segunda",
				"Ter�a","Quarta","Quinta","Sexta","S�bado"};
				
		int dia = Integer.parseInt(args[0]);
		
		System.out.println(diasDasemana[dia]);
		
		System.exit(0);
	}
	
}
