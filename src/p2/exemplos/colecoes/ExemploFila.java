package p2.exemplos.colecoes;

import java.util.ArrayDeque;
import java.util.Queue;

public class ExemploFila {
	
	public static void main(String[] args) {
		Queue<String> fila = new ArrayDeque<>();
		
		fila.add("Icaro");
		fila.add("Jefferson");
		fila.remove();
		System.out.println(fila);
	}

}
