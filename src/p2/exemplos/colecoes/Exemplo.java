package p2.exemplos.colecoes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {
		
		List<String> colecao = new ArrayList<>();
		colecao.add("Segunda");
		colecao.addAll(Arrays.asList("Terça","Quarta"));
		
		for (String dia : colecao) {
			System.out.println(dia);
		}
		System.out.println("---------------");
		
		Iterator<String> iterator = colecao.iterator();
		
		while (iterator.hasNext()) {
			String dia = iterator.next();
			if (dia.equals("Segunda"))
				iterator.remove();
			System.out.println(dia);
		}
		System.out.println(colecao);
		
		
	}
}








