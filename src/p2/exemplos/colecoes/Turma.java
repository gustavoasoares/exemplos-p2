package p2.exemplos.colecoes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class Turma {

	public static void main(String[] args) {
		
		List<String> alunos1 = new ArrayList<>(Arrays.asList("Andre", "Caio",
				"Lucio"));
		List<String> alunos2 = new ArrayList<>(Arrays.asList("Metal", "Rodrigo",
				"MalaB"));
		List<String> alunos3 = new ArrayList<>(Arrays.asList("Mendelssohn", "Igor",
				"Anakin"));
		List<String> alunos4 = new ArrayList<>(Arrays.asList("Wendley", "Vitor",
				"Hector"));

		Map<String, List<String>> turmas = new TreeMap<>();
		turmas.put("turma1", alunos1);
		turmas.put("turma3", alunos3);
		turmas.put("turma4", alunos4);
		turmas.put("turma2", alunos2);
		
		List<String> lista = turmas.get("turma3");
		System.out.println(lista);
		
		for (Entry<String, List<String>> entrada : turmas.entrySet()) {
			System.out.println(entrada.getKey() + ": " + entrada.getValue());
		}
		
		
		
		
		
		
		


	}
}
