package p2.exemplos.colecoes;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import org.omg.CORBA.RepositoryIdHelper;

import p2.exemplos.comum.Pessoa;

public class NomesRepetidos {

	public static void main(String[] args) {

		Pessoa[] listaDePessoas = { new Pessoa("Icaro"), new Pessoa("Icaro"),
				new Pessoa("Felipe"), new Pessoa("Felipe"),
				new Pessoa("Felipe"), new Pessoa("Jefferson"),
				new Pessoa("Jefferson"), new Pessoa("Wendley"), 
				new Pessoa("Mendelssohn") };

		Set<Pessoa> pessoas = new LinkedHashSet<>();
		Set<Pessoa> repetidos = new HashSet<>();
		
		for (Pessoa pessoa : listaDePessoas) {
			if (!pessoas.add(pessoa))
				repetidos.add(pessoa);
		}
		pessoas.removeAll(repetidos);
		
		System.out.println(pessoas);
	}

}
