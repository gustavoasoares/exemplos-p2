package p2.exemplos.comum;

import java.util.List;


public class Pessoa implements Comparable<Pessoa>  {

	private int idade;
	private String nome;
	private String cpf;
	private List<String> hobbies;
	private List<String> filmesFavoritos;
	
	public Pessoa(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public Pessoa(String nome) {
		this.nome = nome;
		
	}
	
	public Pessoa(String nome, int idade) {
		this.nome = nome;
		this.idade = idade;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getCpf() {
		return this.cpf;
	}
	
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
	public boolean eMaisVelhaQue(Pessoa pessoa) {
		return this.idade > pessoa.idade;
	}

	public String toString() {
		return this.nome;
	}
	
	public static Pessoa pessoaMaisVelha(Pessoa[] pessoas) {
		int indexMaior = 0;
		for (int indexPessoa = 1; indexPessoa < pessoas.length; indexPessoa++) {
			
			Pessoa pessoaAtual = pessoas[indexPessoa];
			if (pessoaAtual.eMaisVelhaQue(pessoas[indexMaior])) {
				indexMaior = indexPessoa;
			}
		}
		return pessoas[indexMaior];
	}

	public List<String> getHobbies() {
		return hobbies;
	}

	public void setHobbies(List<String> hobbies) {
		this.hobbies = hobbies;
	}

	public List<String> getFilmesFavoritos() {
		return filmesFavoritos;
	}

	public void setFilmesFavoritos(List<String> filmesFavoritos) {
		this.filmesFavoritos = filmesFavoritos;
	}

	@Override
	public int compareTo(Pessoa pessoa) {
		return this.nome.compareTo(pessoa.getNome());
	}
}
