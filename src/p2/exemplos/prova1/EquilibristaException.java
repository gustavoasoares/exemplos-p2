package p2.exemplos.prova1;

public class EquilibristaException extends Exception {

	public EquilibristaException(String message) {
		super(message);
	}

}
