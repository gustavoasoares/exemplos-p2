package p2.exemplos.prova1;

public class Equilibrista {
	public final static int TAMANHO_MAX_CORDA = 10;
	private int posicaoAtual;
	private String simbolo;
	private String[] corda;
	
	public Equilibrista(int tamanho, String simbolo) throws EquilibristaException{
		this.simbolo = simbolo;
		posicaoAtual = 0;
		corda = iniciaCorda(tamanho);
	}
	
	private String[] iniciaCorda(int tamanho) throws EquilibristaException{
		if (tamanho <= 0)
			throw new EquilibristaException("Tamanho tem que ser positivo");
		if(tamanho > TAMANHO_MAX_CORDA){tamanho = TAMANHO_MAX_CORDA;}
		String[] corda = new String[tamanho];
		corda[0] = simbolo;
		for (int i = 1; i < corda.length; i++) {
			corda[i] = "_";
		}
		return corda;
	}
	
	public int getPosicaoAtual(){ return posicaoAtual; }
	
public String getSimbolo(){ 	return simbolo;}

public int getTamanhoCorda(){ 	return corda.length;}

	@Override
	public String toString() {
		String resultado = "";
		for (int i = 0; i < corda.length; i++) {
			resultado += (corda[i] + " ");
		}
		return resultado;
	}

	public void move(int passos) {
		this.corda[posicaoAtual] = "_";
		
		this.posicaoAtual += passos;
		if (this.posicaoAtual > corda.length - 1)
			this.posicaoAtual = (this.posicaoAtual % corda.length); 
		
		this.corda[posicaoAtual] = this.simbolo;
	}

	
	@Override
	public boolean equals(Object objetoComparado) {
		if (!(objetoComparado instanceof Equilibrista))
			return false;
		Equilibrista equilibristaComparado = (Equilibrista) objetoComparado;
		if (getTamanhoCorda() != equilibristaComparado.getTamanhoCorda())
			return false;
		
		if(!getSimbolo().equals(equilibristaComparado.getSimbolo()))
			return false; 
		
		return true;
	}
}

