package p2.exemplos.interfaces;

import java.util.List;

public interface Busca {
	public int buscaNumero(List<Integer> lista, int numero);
}
