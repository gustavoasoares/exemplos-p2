package p2.exemplos.interfaces;

import java.util.ArrayList;
import java.util.List;

public class Pilha<E> implements IPilha<E> {

		private List<E> lista = new ArrayList<>();
	
		@Override
		public boolean push(E element) {
			return lista.add(element);
		}
		
		@Override
		public E pop() {
			if (lista.isEmpty())
				return null;
			return lista.remove(lista.size()-1);
		}
		
		@Override
		public String toString() {
			return lista.toString();
		};
 	
	
		public static void main(String[] args) {
		IPilha<String> pilha = new Pilha<>();
		pilha.push("Item1");
		pilha.push("Item2");
		pilha.push("Item3");
		System.out.println(pilha);
		System.out.println("removendo último elemento...");
		pilha.pop();
		System.out.println(pilha);
		
	}
	
}
