package p2.exemplos.interfaces;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import p2.exemplos.comum.Pessoa;

public class ConjuntoEspecial<E> implements Set<E>  {

	private int count = 0;

	private Set<E> conjunto = new HashSet<>();
	
	public ConjuntoEspecial() {
	}
	
	public ConjuntoEspecial(Set conjunto) {
		this.conjunto = conjunto;
	}
	
	public boolean add(E element) {
		count++;
		return conjunto.add(element);
	}

	public boolean addAll(Collection<? extends E> c) {
		count += c.size();
		return conjunto.addAll(c);
	}

	public int getCount() {
		return count;
	}	

	@Override
	public void clear() {
		conjunto.clear();
	}

	@Override
	public boolean contains(Object o) {
		return conjunto.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return conjunto.containsAll(c);
	}

	@Override
	public boolean isEmpty() {
		return conjunto.isEmpty();
	}

	@Override
	public Iterator<E> iterator() {
		return conjunto.iterator();
	}

	@Override
	public boolean remove(Object o) {
		return conjunto.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return conjunto.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return conjunto.retainAll(c);
	}

	@Override
	public int size() {
		return conjunto.size();
	}

	@Override
	public Object[] toArray() {
		return conjunto.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return conjunto.toArray(a);
	}
	
	public static void main(String[] args) {
		Set<Pessoa> conjunto = new ConjuntoEspecial<>(new HashSet<Pessoa>());
		
		conjunto.add(new Pessoa("Gustavo"));
		System.out.println(conjunto.contains(new Pessoa("Gustavo")));
		System.out.println(((ConjuntoEspecial<Pessoa>) conjunto).getCount());
	}

}
