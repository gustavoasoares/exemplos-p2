package p2.exemplos.interfaces;

public interface IPilha<E> {

	public boolean push(E element);
	
	public E pop();
}
