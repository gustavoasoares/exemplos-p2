package p2.exemplos.interfaces;

import java.util.List;

public class BuscaSequencial implements Busca {

	public int buscaNumero(List<Integer> lista, int numero) {
		int resultado = -1;
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).equals(numero))
				resultado = i;
		}
		return resultado;
	}
}
