package p2.exemplos.interfaces;

import java.util.ArrayList;
import java.util.List;

public class Programa {
	
	private List<Integer> lista;
	private Busca busca;
	
	public Programa(Busca busca, List<Integer> lista) {
		this.busca = busca;
		this.lista = lista;
	}
	
	public void run() {
		int x = 10;
		int index = busca.buscaNumero(lista, x);
		if (index < 0)
			System.out.println("O elemento não está na lista");
		else 
			System.out.println("O elemento está no índice: " + index);
	}
	
	public static void main(String[] args) {
		List<Integer> lista = new ArrayList<>();
		lista.add(5);
		lista.add(10);
		Busca busca = new BuscaSequencial();
		
		Programa program = new Programa(busca, lista);
		program.run();
	}
}
