package p2.exemplos.interfaces.estrategia;

import java.util.ArrayList;
import java.util.List;

public class ItemsImportados {
	
	private List<Item> items = new ArrayList<>();
	
	private double total;

	public boolean adiciona(Item item) {
		total += item.getValor();
		return items.add(item);
	}
	
	public double getTotal() {
		return total;
	}

	public double calculaImposto(double valor, CalculoDeImposto imposto) {
		return imposto.calcula(valor);
	}

}
