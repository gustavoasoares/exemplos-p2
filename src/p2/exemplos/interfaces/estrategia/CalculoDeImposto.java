package p2.exemplos.interfaces.estrategia;

public interface CalculoDeImposto {
	
	double calcula(double valorDosItems);

}
