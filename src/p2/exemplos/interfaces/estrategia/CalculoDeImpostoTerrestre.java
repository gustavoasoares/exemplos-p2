package p2.exemplos.interfaces.estrategia;

public class CalculoDeImpostoTerrestre implements CalculoDeImposto {

	public static CalculoDeImposto instance = new CalculoDeImpostoTerrestre();

	private CalculoDeImpostoTerrestre() {

	}

	@Override
	public double calcula(double valorDosItems) {
		double resultado = 0;
		double diferenca = valorDosItems - 300;
		if (diferenca > 0)
			resultado = diferenca * 0.5;
		return resultado;
	}

}
