package p2.exemplos.heranca;

import java.util.List;

public class ModoGrid extends ModoDeExibicao{

	private int colunas = 2;
	public ModoGrid() {
		
	}
	
	@Override
	public String exibe(List<Filme> filmes) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < filmes.size(); i++) {
			sb.append(filmes.get(i));
			if (i < filmes.size() - 1) {
				if ((i+1) % this.colunas == 0) {
					sb.append("\n");	
				} else { 
					sb.append(" - ");	
				}	
			}
		}
		return sb.toString();
	}

}
