package p2.exemplos.heranca;

import java.util.ArrayList;
import java.util.List;

public class Filme {
	
	private String nome;
	
	public Filme(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}

	public static List<Filme> pegaTodosOsFilmes() {
		List<Filme> filmes = new ArrayList<>();
		filmes.add(new Filme("A volta dos que não foram"));
		filmes.add(new Filme("Poeira no fundo do mar"));
		filmes.add(new Filme("A lagoa azul"));
		filmes.add(new Filme("titanic"));
		filmes.add(new Filme("O auto da compadecida"));
		return filmes;
	}

}
