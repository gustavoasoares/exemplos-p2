package p2.exemplos.heranca;

import java.util.List;
import java.util.Scanner;

public class Cinema {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int opcao;
		ModoDeExibicao modo = null;
		List<Filme> filmes = Filme.pegaTodosOsFilmes();

		do {
			System.out.println("1 - Selecione o modo de exibição");
			System.out.println("2 - Exibir a lista de filmes");
			opcao = sc.nextInt();

			switch (opcao) {
			case 1:
				List<String> modos = ModoDeExibicao.getModos();
				for (int i = 0; i < modos.size(); i++) {
					System.out.println((i + 1) + " - " + modos.get(i));
				}
				int modoId = sc.nextInt();
				try {
					modo = ModoDeExibicao.build(modos.get(modoId - 1));
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				System.out.println(modo.exibe(filmes));
				break;
			default:
				break;
			}

		} while (opcao != -1);

	}

}
