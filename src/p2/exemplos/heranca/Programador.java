package p2.exemplos.heranca;

public class Programador extends Funcionario{

	private int horasTrabalhadas;
	private double valorHora;
	
	public Programador(String nome, String matricula, double valorHora, int horasTrabalhadas) {
		super(nome,matricula);
		this.valorHora = valorHora;
		this.horasTrabalhadas = horasTrabalhadas;
	}
	
	@Override
	public double calculaSalario() {
		return valorHora * horasTrabalhadas;
	}
	
	@Override
	public String toString() {
		
		String resultado = super.toString();
		resultado += "\nValor da hora: " + this.valorHora +
				"\nHoras trabalhadas: " + this.horasTrabalhadas;
		return resultado;
	}
	
	
}
