package p2.exemplos.heranca;

import java.io.Serializable;

public class Empresa implements Pagavel {
	private String nome;
	private String cnpj;
	private double valor;
	
	public Empresa(String nome, String cnpj, double valor) {
		this.nome = nome;
		this.cnpj = cnpj;
		this.valor= valor;
	}
	
	public String getNome() {
		return nome;
	}
	
	public double getValor() {
		return this.valor;
	}

	@Override
	public double calculaValor() {
		return this.getValor();
	}
	
}
