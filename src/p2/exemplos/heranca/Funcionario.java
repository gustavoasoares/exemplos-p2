package p2.exemplos.heranca;

import p2.exemplos.comum.Pessoa;

public abstract class Funcionario extends Pessoa implements Pagavel {
	
	private String matricula;
	
	
	public Funcionario(String nome, String matricula) {
		super(nome);
		this.matricula = matricula;
	}
		
	public String getMatricula() {
		return matricula;
	}
	
	public abstract double calculaSalario();

	@Override
	public String toString() {
		return " Matricula:" + matricula;
	}

	@Override
	public double calculaValor() {
		return this.calculaSalario();
	}
}
