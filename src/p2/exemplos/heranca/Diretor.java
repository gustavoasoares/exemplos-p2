package p2.exemplos.heranca;

public class Diretor extends Funcionario {

	
	private double salarioBase;
	private double lucro;
	
	public Diretor(String nome, String matricula, double salarioBase, double lucro) {
		super(nome,matricula);
		this.lucro = lucro;
		this.salarioBase = salarioBase;
	}
	
	public double calculaSalario() {
		return lucro + salarioBase;
	}
}
