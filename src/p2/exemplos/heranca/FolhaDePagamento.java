package p2.exemplos.heranca;

import java.util.ArrayList;
import java.util.List;

public class FolhaDePagamento {

	public static void main(String[] args) {
		List<Pagavel> funcionarios = getFuncionarios();

		Empresa empresa1 = new Empresa("Dona ines", "5242342", 100.0);
		Empresa empresa2 = new Empresa("CU", "423424", 200.0);
		
		List<Pagavel> itemsPagaveis = new ArrayList<>(funcionarios);
		itemsPagaveis.add(empresa1);
		itemsPagaveis.add(empresa2);
		
		
		
		double valorTotal = 0;
		for (Pagavel item: itemsPagaveis) {
			System.out.println(item.toString());
			valorTotal += item.calculaValor();
		}
		
		System.out.println("Total pago: " + valorTotal  );
	}

	private static List<Pagavel> getFuncionarios() {
		List<Pagavel> funcionarios = new ArrayList<>();
		funcionarios.addAll(getProgramadores());
		funcionarios.addAll(getDiretores());
		return funcionarios;
	}

	private static List<Pagavel> getDiretores() {
		List<Pagavel> diretores = new ArrayList<>();

		Diretor gustavo = new Diretor("Gustavo", "3", 14000, 3000);
		diretores.add(gustavo);
		return diretores;
	}

	private static List<Funcionario> getProgramadores() {
		List<Funcionario> programadores = new ArrayList<>();

		Programador caio = new Programador("Caio", "1", 11, 250);
		Programador alex = new Programador("Alex", "2", 8, 20);

		programadores.add(caio);
		programadores.add(alex);

		return programadores;
	}

}
