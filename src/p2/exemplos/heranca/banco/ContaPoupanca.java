package p2.exemplos.heranca.banco;

public class ContaPoupanca extends ContaBancaria {

	public ContaPoupanca(String cpf, String nome, double saldoInical) {
		super(cpf, nome, saldoInical);
	}
	
	@Override
	public void sacar(double valor) throws Exception {
		if (valor > this.getSaldo()) {
			throw new Exception("Saldo insuficiente");
		}
		super.sacar(valor);
	}
	
	public void adicionarJuros() {
		
	}
	
	
}
