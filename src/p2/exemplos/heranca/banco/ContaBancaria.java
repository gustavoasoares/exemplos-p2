package p2.exemplos.heranca.banco;

import java.util.ArrayList;

import p2.exemplos.comum.Pessoa;

public class ContaBancaria {
	
	private static final String ERRO_VALOR_NAO_POSITIVO = "Valor tem que ser positivo";
	private double saldo; 
	private Pessoa titular;
	
	public ContaBancaria(String cpf, String nome, double saldoInical ) {
		this.saldo = saldoInical;
		titular = new Pessoa(nome, cpf);
	}
	
	//todo adicionar tratamento de exce��o
	//adicionar transacoes
	public void depositar(double valor) {
		saldo += valor;
	}
	
	public void sacar(double valor) throws Exception {
		if (valor <= 0)
			throw new Exception(ERRO_VALOR_NAO_POSITIVO);
		saldo -= valor;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void descontarTaxa(double valor) {
		saldo -= valor;
	}
	
	public void transferir(double valor, ContaBancaria conta) throws Exception {
		if (valor <= 0)
			throw new Exception(ERRO_VALOR_NAO_POSITIVO);
		saldo -= valor;
		conta.depositar(valor);
		
	}
	

}
