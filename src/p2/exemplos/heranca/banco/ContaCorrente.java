package p2.exemplos.heranca.banco;

public class ContaCorrente extends ContaBancaria {

	
	private int quantidadeDeOperacoes; 
	public ContaCorrente(String cpf, String nome, double saldoInical) {
		super(cpf, nome, saldoInical);
	}

	@Override
	public void sacar(double valor) throws Exception {
		super.sacar(valor);
		quantidadeDeOperacoes++;
		if (quantidadeDeOperacoes > 3) {
			descontarTaxa(2);
		}
		
	}
	
	public static void main(String[] args) {
		
		ContaBancaria conta = new ContaCorrente("134123", "Jefferson", 0);	
		try {
			conta.sacar(10);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
