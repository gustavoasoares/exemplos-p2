package p2.exemplos.heranca;

import java.util.ArrayList;
import java.util.List;

public abstract class ModoDeExibicao {

	public static List<String> getModos() {
		List<String> result = new ArrayList<>();
		result.add("ModoVertical");
		result.add("ModoHorizontal");
		result.add("ModoGrid");
		return result;
	}

	public static ModoDeExibicao build(String className)
			throws ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		Class<ModoDeExibicao> classModoDeExibicao = (Class<ModoDeExibicao>) Class
				.forName("p2.exemplos.heranca." + className);
		return classModoDeExibicao.newInstance();

	}

	public abstract String exibe(List<Filme> filmes);

}
