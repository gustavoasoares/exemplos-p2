package p2.exemplos.heranca;

import java.util.List;

public class ModoHorizontal extends ModoDeExibicao {

	@Override
	public String exibe(List<Filme> filmes) {
		StringBuilder result = new StringBuilder();
		for (Filme filme : filmes) {
			result.append(filme);
			result.append(" - ");
		}
		return result.toString();
	}

	
}
