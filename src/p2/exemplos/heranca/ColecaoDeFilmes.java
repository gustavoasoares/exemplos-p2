package p2.exemplos.heranca;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ColecaoDeFilmes {
	
	private List<Filme> filmes = new LinkedList<>();

	public ColecaoDeFilmes(List<Filme> filmes) {
		this.filmes = filmes;
	}

	public void adicionar(Filme filme) {
		this.filmes.add(filme);
	}
	
	
	public static void main(String[] args) {
		ArrayList<Filme> filmes = new ArrayList<>();
		filmes.add(new Filme("A volta dos que não foram"));
		filmes.add(new Filme("Poeira no fundo do mar"));
		filmes.add(new Filme("A lagoa azul"));
		filmes.add(new Filme("titanic"));
		filmes.add(new Filme("O auto da compadecida"));
		
		ColecaoDeFilmes colecaoDeFilmes = new ColecaoDeFilmes(filmes);
		
		
	
	}

}
