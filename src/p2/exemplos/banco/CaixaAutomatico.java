package p2.exemplos.banco;

import java.util.Scanner;

public class CaixaAutomatico {
	
	private static final int SAIR = 4;
	private static final int SALDO = 3;
	private static final int DEPOSITAR = 2;
	private static final int SACAR = 1;
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {

		
		Conta conta = LerNovaConta();
		
		boolean continua = true;
		while (continua) {
			System.out.println("1 - Sacar\n2 - Depositar"
					+ "\n3 - Saldo\n4 - Sair");
			System.out.println("Digite uma op��o");
			int opcao = sc.nextInt();
			
			switch (opcao) {
			case SACAR:
				sacar(conta);
				break;
			case DEPOSITAR: 
				depositar(conta);
				break;
			case SALDO : 
				visualizarSaldo(conta);
				break;
			case SAIR : 
				continua = false;
				break;
			default:
				break;
			}	
		}
	}

	private static void visualizarSaldo(Conta conta) {
		System.out.println(
				"O saldo de " + conta.getNome() + " �: " +
				conta.getSaldo() + " reais");
		System.out.println("Operacao finalizada.");
	}

	private static void depositar(Conta conta) {
		System.out.println("Digite o valor");
		double valorDeposito = sc.nextDouble();
		if (!conta.depositar(valorDeposito))
			System.err.println("O valor depositado "
					+ "tem que ser positivo\n");
		System.out.println("Operacao finalizada.");
	}

	private static void sacar(Conta conta) {
		System.out.println("Digite o valor:");
		double valorSaque = sc.nextDouble();
		try {
			conta.sacar(valorSaque);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			System.out.println("Operacao finalizada.");
		}
		
	}

	private static Conta LerNovaConta() {
		sc = new Scanner(System.in);
		System.out.println("Digite o nome do titular: ");
		String nome = sc.next();
		System.out.println("Digite o cpf do titular:");
		String cpf = sc.next();
		System.out.println("Digite o saldo inicial: ");
		double saldo = sc.nextDouble();		
		
		Conta conta = new Conta(1, nome, cpf, saldo);
		return conta;
	}

}
