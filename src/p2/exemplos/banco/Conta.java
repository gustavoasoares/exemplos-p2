package p2.exemplos.banco;

import p2.exemplos.comum.Pessoa;

public class Conta {

	private int id;
	private double saldo;
	private Pessoa titular;

	public Conta(int id, String nome, String cpf, double saldo) {
		this.id = id;
		this.titular = new Pessoa(nome, cpf);
		this.saldo = saldo;
	}

	public String getNome() {
		return titular.getNome();
	}

	public double getSaldo() {
		return this.saldo;
	}

	public boolean depositar(double valor) {
		if (valor <= 0)
			return false;
		this.saldo += valor;
		return true;
	}

	public void sacar(double valor) throws Exception {
		if (valor <= 0)
			throw new Exception("O valor tem que ser positivo.");
		if (this.saldo < valor)
			throw new Exception("Saldo Insuficiente");

		this.saldo -= valor;
	}

}
