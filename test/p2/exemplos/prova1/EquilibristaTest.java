package p2.exemplos.prova1;

import static org.junit.Assert.*;

import org.junit.Test;

public class EquilibristaTest {

	@Test
	public void testMove() throws EquilibristaException {
		
		if ( 1 > Equilibrista.TAMANHO_MAX_CORDA)
			System.out.println();
		
		Equilibrista fabiana = new Equilibrista(10, "f");
		fabiana.move(3);
		
		assertEquals("_ _ _ f _ _ _ _ _ _ ", fabiana.toString());
		assertEquals(3, fabiana.getPosicaoAtual());
		fabiana.move(9);
		assertEquals("_ _ f _ _ _ _ _ _ _ ", fabiana.toString());
		assertEquals(2,fabiana.getPosicaoAtual());
		fabiana.move(20);
		assertEquals("_ _ f _ _ _ _ _ _ _ ", fabiana.toString());
		assertEquals(2,fabiana.getPosicaoAtual());
	}
	
	@Test(expected = EquilibristaException.class)
	public void testIniciaCordaTamanhoNegativo() throws EquilibristaException {
		Equilibrista fabiana =  new Equilibrista(-10,"f");
	}
	
	@Test
	public void testEquals() throws EquilibristaException {
		Equilibrista flabio = new Equilibrista(10, "f");
		Equilibrista fabiana = new Equilibrista(10, "f");
		assertTrue(flabio.equals(fabiana));
	}

}
