package p2.exemplos.conta;

import static org.junit.Assert.*;

import org.junit.Test;

import p2.exemplos.banco.Conta;

public class ContaTest {

	@Test
	public void testSacarSaldoInsuficiente() {
		
		Conta conta = new Conta(1,"Alex","355234",0);
		
		assertEquals("Alex", conta.getNome());
		conta.depositar(10);
		assertEquals(10, conta.getSaldo(), 0);
		try {
			conta.sacar(20);
			fail("exce��o de saldo insuficiente era esperada");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = Exception.class)	
	public void testSacarValorNegativo() throws Exception {
		Conta conta = new Conta(1,"Alex","355234",0);
		conta.depositar(10);
		conta.sacar(-20);
	}
	
	@Test
	public void testEquals() {
		Conta conta = new Conta(1,"Alex","355234",0);
		Conta conta2 = new Conta(1,"Alex","355234",0);
		
		assertTrue(conta.equals(conta2));
	}
	
	
	
	
}
