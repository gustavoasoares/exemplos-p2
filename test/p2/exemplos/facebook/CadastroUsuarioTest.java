package p2.exemplos.facebook;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import p2.exemplos.facebook.CadastroDeUsuario;
import p2.exemplos.facebook.Usuario;


public class CadastroUsuarioTest {

	private CadastroDeUsuario cadastro;
	private Usuario usuario;
	
	@Before
	public void iniciaCadastro() {
		cadastro = new CadastroDeUsuario(2);
		usuario = new Usuario("Messi","messi@barcelona.es","BrasilHexa");
		
	}
	
	@Test
	public void testCadastraUsuario() throws Exception {
		
		cadastro.adicionaUsuario(usuario);

		Usuario usuarioEsperado = new Usuario("Messi","messi@barcelona.es","BrasilHexa");
		assertEquals(usuarioEsperado,cadastro.buscaUsuarioPorEmail("messi@barcelona.es"));
	}
	
	@Test(expected = Exception.class) 
	public void testCadastraUsuarioJaExistente() throws Exception {

		cadastro.adicionaUsuario(usuario);
		Usuario usuarioRepetido = 
				new Usuario("Messi","messi@barcelona.es","BrasilHexa");
		cadastro.adicionaUsuario(usuarioRepetido);
		
	}
	

}
