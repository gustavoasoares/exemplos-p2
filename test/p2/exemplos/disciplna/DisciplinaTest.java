package p2.exemplos.disciplna;

import static org.junit.Assert.*;

import org.junit.Test;

import p2.exemplos.disciplina.Disciplina;

public class DisciplinaTest {

	@Test
	public void testCalculaMedia() {
		
		Disciplina p2 = new Disciplina("Programacao 2");
		assertEquals("Programacao 2", p2.getNome());
		
		
		p2.adicionaNota(4);
		p2.adicionaNota(3);
		p2.adicionaNota(5);
		
		double media = p2.calculaMedia();
		assertEquals(4, media, 2);
		
	}

}
